﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  public float moveSpeed { get; set; } = 10;
  public float jumpHeight { get; set; }
  public float gravity { get; set; }

  public float horizontalSpeed { get; set; }
  public float verticalSpeed { get; set; }

  public float pixelSize = 0.02f;

  public LayerMask Solid;

  private bool KeyLeft;
  private bool KeyRight;
  private bool KeyUp;
  private bool KeyDown;
  private bool KeyJumb;
  private bool KeyAction;

  private Vector2 botLeft;
  private Vector2 botRight;
  private Vector2 topLeft;
  private Vector2 topRight;

  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    CalculateBounds();

    Debug.Log("Collision ahead" + CheckCollision(botLeft,Vector2.down, 2, Solid) + "distance: " + CheckCollisionDistance(botLeft, Vector2.down, 2, Solid));

    GetInput();
    Move();
  }

  private void GetInput()
  {
    KeyLeft = Input.GetKey(KeyCode.LeftArrow);
    KeyRight = Input.GetKey(KeyCode.RightArrow);
    KeyUp = Input.GetKey(KeyCode.UpArrow);
    KeyDown = Input.GetKey(KeyCode.DownArrow);
    KeyJumb = Input.GetKey(KeyCode.Z);
    KeyAction = Input.GetKey(KeyCode.X);
  }

  private void Move()
  {
    if(KeyLeft)
    {
      horizontalSpeed = -moveSpeed * Time.deltaTime;
    }
    else if (KeyRight)
    {
      horizontalSpeed = moveSpeed * Time.deltaTime;
    }
    else if ((!KeyLeft && !KeyRight) || (KeyLeft && KeyRight))
    {
      horizontalSpeed = 0;
    }

    transform.position = new Vector2(
      transform.position.x + horizontalSpeed,
      transform.position.y + verticalSpeed
      );
  }

  private bool CheckCollision(Vector2 raycastOrigin, Vector2 direction, float distance,LayerMask layer)
  {
    return Physics2D.Raycast(raycastOrigin, direction, distance, layer);
  }

  private float CheckCollisionDistance(Vector2 raycastOrigin, Vector2 direction, float distance, LayerMask layer)
  {
    int i = 0;

    while(Physics2D.Raycast(raycastOrigin, direction, distance, layer))
    {
      i++;

      if(distance > pixelSize)
      {
        distance -= pixelSize;
      }
      else
      {
        distance = pixelSize;
      }

      if(i> 1000)
      {
        return 0;
      }
    }

    return distance;
  }

  private void CalculateBounds()
  {
    Bounds b = GetComponent<BoxCollider2D>().bounds;
    topLeft = new Vector2(b.min.x, b.max.y);
    botLeft = new Vector2(b.min.x, b.min.y);
    topRight = new Vector2(b.max.x, b.max.y);
    botRight = new Vector2(b.max.x, b.min.y);
  }
}
